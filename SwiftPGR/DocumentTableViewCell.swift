//
//  DocumentTableViewCell.swift
//  SwiftPGR
//
//  Created by Kevin DeRonne on 1/12/16.
//  Copyright © 2016 Clarence Westberg. All rights reserved.
//

import UIKit

class DocumentTableViewCell: UITableViewCell {

    @IBOutlet weak var snippetLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    

}
