//
//  SearchResult+CoreDataProperties.swift
//  SwiftPGR
//
//  Created by Kevin DeRonne on 1/14/16.
//  Copyright © 2016 Clarence Westberg. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SearchResult {

    @NSManaged var id: NSNumber?
    @NSManaged var title: String?
    @NSManaged var searchResultToSnippets: NSSet?

}
