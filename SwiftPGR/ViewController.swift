//
//  ViewController.swift
//  SwiftPGR
//
//  Created by Clarence Westberg on 12/16/15.
//  Copyright © 2015 Clarence Westberg. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var resultsTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    //var results = [String()]
    var docs    = [Int():String()]
    var results = [NSManagedObject]()
    
    internal func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    internal func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = resultsTable.dequeueReusableCellWithIdentifier("Cell")! as! DocumentTableViewCell
        if results.count > indexPath.row {
            //cell.textLabel?.text = results[indexPath.row]
            //cell.textLabel?.text = results[indexPath.row].valueForKey("searchResultToSnippets")! as! String
            //NSSet(results[indexPath.row].valueForKey("searchResultToSnippets"))
            
            let attrStr = try! NSAttributedString(
                data: results[indexPath.row].valueForKey("title")!.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!,
                options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil)
            
            cell.snippetLabel?.attributedText = attrStr
        }
        else {
            cell.textLabel?.text = "cellForRowAtIndexPath called before data in results array"
        }
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //addDocumentSnippetToTable(0, sec: 0, document: ["This":"Stuff"])
        
        // Here is where I suspsect a lot more MVC should come in
        resultsTable.delegate   = self
        resultsTable.dataSource = self
        searchBar.delegate      = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func deleteAllData(entity: String)
    {
        let appDelegate    = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest   = NSFetchRequest(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.deleteObject(managedObjectData)
                appDelegate.saveContext()
                
            }
            // yourTableView = []
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
    func saveResult(title: String, id: Int, snippet: [String]) {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("SearchResult",
            inManagedObjectContext:managedContext)
        
        let document = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedContext)
        
        //3
        document.setValue(title, forKey: "title")
        document.setValue(id, forKey: "id")
        
        
        // Create a snippet object in the correct contex via a snippet entity
        
        let snippetEntity = NSEntityDescription.entityForName("Snippet", inManagedObjectContext: managedContext)
        
        // Get the relationship to use to link the snippets
        let snippets = document.valueForKeyPath("searchResultToSnippets") as! NSMutableSet
        
        for snippetString in snippet {
            // Create snippet object in coreData
            let snippetObject = NSManagedObject(entity: snippetEntity!, insertIntoManagedObjectContext: managedContext)
            // Set snippet content
            snippetObject.setValue(snippetString, forKey: "snippetText")
            // Set the relationship
            snippets.addObject(snippetObject)
        }
        
        //4
        do {
            try managedContext.save()
            //5
            results.append(document)
            //saveSnippet(snippet)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        // indent=true probably isn't necessary but if we need to print for debug it's handy.
        let url = NSURL(string: "http://hm12.nxn.com:8983/solr/engrafa-allparagraphs/select?q="+searchText+"&fl=id%2Ctitle&df=all_fields&wt=json&indent=true&hl=true&hl.fl=paragraph_text&hl.simple.pre=%3Cem%3E&hl.simple.post=%3C%2Fem%3E")
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
            
            do {
                let json     = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                
                if let resp  = json["response"]!!["docs"] as? [[String: AnyObject]] {
                    // Reset results array
                    self.results.removeAll()
                    for doc in resp {
                        
                        if let title = doc["title"] {
                            // Add results to table
                            //iceprint(doc["id"])
                            //self.results.append(title as! String)
                            self.saveResult(title as! String, id: Int(doc["id"] as! String)!, snippet: [""])
                        }
                    }
                }
//                if let test = json["highlighting"] {
                   //print("\(_stdlib_getDemangledTypeName(test!))")
                    //print(test!["206090"])
                //print(test[206090])
  //              }
//                if let highlight = json["highlighting"] as? [String:[String:[String]]] {
//                    for (key,value) in highlight {
//                        //print(key)
//                        //print(value["paragraph_text"]![0])
//                    }
//                }
                
                
                if NSThread.isMainThread() {
                    self.resultsTable.reloadData()
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), {
                        self.resultsTable.reloadData()
                    })
                }
            }
            catch {
                print("error serializing JSON: \(error)")
            }
          
        }
        task.resume()
    }

}

